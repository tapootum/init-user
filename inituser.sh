#!/bin/bash
adduser tapootum
echo "tapootum ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/tapootum
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl restart sshd
apt -y update
yum -y update
apt -y install vim tmux git
yum -y install vim tmux git

#useradd -s /bin/bash -d /opt/stack -m stack
#echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack